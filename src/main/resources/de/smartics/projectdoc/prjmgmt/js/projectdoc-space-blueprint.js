/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
AJS.bind("blueprint.wizard-register.ready", function () {

	// unused right now
    function submitProjectdocSpace(e, state) {
        state.pageData.ContentPageTitle = state.pageData.name;
        return Confluence.SpaceBlueprint.CommonWizardBindings.submit(e, state);
    }

	// unused right now
    function preRenderProjectdocSpace(e, state) {
        state.soyRenderContext['atlToken'] = AJS.Meta.get('atl-token');
        state.soyRenderContext['showSpacePermission'] = false;
    }

    Confluence.Blueprint.setWizard('de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-prjmgmt:projectdoc-space-blueprints-item-prjmgmt', function(wizard) {
        wizard.on("pre-render.spaceBasicDetailsId", Confluence.SpaceBlueprint.CommonWizardBindings.preRender);
        wizard.on("post-render.spaceBasicDetailsId", Confluence.SpaceBlueprint.CommonWizardBindings.postRender);
        wizard.on("submit.spaceBasicDetailsId", Confluence.SpaceBlueprint.CommonWizardBindings.submit);
    });
});
