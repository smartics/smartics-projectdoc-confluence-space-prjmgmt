/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-prjmgmt:create-doctype-template-alternative',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-prjmgmt:create-doctype-template-alternative-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-prjmgmt:create-doctype-template-decision',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-prjmgmt:create-doctype-template-decision-type',
		PROJECTDOC.standardWizard);

(function ($) {
    function bindFields(ev, state) {
        $('#day-date').datepicker({
            dateFormat : "yy-mm-dd"
        });
        var title = state.wizardData.title;
        if(title) {
          $('#projectdoc\\.doctype\\.common\\.name').val(title);
          $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
        }

        wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
    }

    Confluence.Blueprint.setWizard('de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-prjmgmt:create-doctype-template-minutes', function(wizard) {
		wizard.on('pre-render.page1Id', function(e, state) {
			state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);
		});
        wizard.on("post-render.page1Id", bindFields);
        wizard.on('submit.page1Id', function(e, state) {
            var name = state.pageData["projectdoc.doctype.common.name"];
            if (!name) {
                alert('Please provide a name for this document.');
                return false;
            }

            var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
            if (!shortDescription){
                alert('Please provide a short description for the document.');
                return false;
            }

            PROJECTDOC.adjustToLocation(state);
        });
    });
})(AJS.$);


Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-prjmgmt:create-doctype-template-minutes-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-prjmgmt:create-doctype-template-open-issue',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-prjmgmt:create-doctype-template-open-issue-severity',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-prjmgmt:create-doctype-template-open-issue-status',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-prjmgmt:create-doctype-template-open-issue-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-prjmgmt:create-doctype-template-release-note',
		  function(wizard) {
		    wizard.on('pre-render.page1Id', function(e, state) {
		      state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);
		    });

		    wizard.on('submit.page1Id', function(e, state) {
		          var version = state.pageData["projectdoc.doctype.release-note.version"];
		          if (!version) {
		              alert('Please provide a version for this release.');
		              return false;
		          }

		          var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
		          if (!shortDescription) {
		              alert('Please provide a short description for the release.');
		              return false;
		          }

		          PROJECTDOC.adjustToLocation(state);
		    });

		      wizard.on('post-render.page1Id', function(e, state) {
		        var title = state.wizardData.title;
		        if(title) {
		          $('#projectdoc\\.doctype\\.release-note\\.version').val(title);
		          $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
		        }
		      });

		    wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
		   }
);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-prjmgmt:create-doctype-template-release-note-type',
		PROJECTDOC.standardWizard);
